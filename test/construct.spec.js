const construct = require('../').construct;
const assert = require('assert');

describe('construct', ()=>{
  it('should not be undefined', ()=>{
    assert.notStrictEqual(typeof construct, 'undefined');
  });
  it('should be a function', ()=>{
    assert.strictEqual(typeof construct, 'function');
  });
  it('should accept 3 arguments', ()=>{
    assert.strictEqual(construct.length, 3);
  });
  describe('Map', ()=>{
    const target = [
      [['key', 'value']],
    ];
    it('should call callback with null for error', ()=>{
      construct(target, [{
        keypath: '0',
        constructor: Map
      }], (e)=>{
        assert.strictEqual(e, null);  
      });
    });
    it('should call callback with an array containing a Map', ()=>{
      construct(target, [{
        keypath: '0',
        constructor: Map
      }], (e, array)=>{
        assert.strictEqual(array[0] instanceof Map, true);
      });
    });
    it('should call callback with an array containing a Map with correct values', ()=>{
      construct(target, [{
        keypath: '0',
        constructor: Map
      }], (e, array)=>{
        assert.strictEqual(array[0].get('key'), 'value');
      });
    });
  });
});