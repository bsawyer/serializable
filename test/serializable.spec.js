const serializable = require('../').serializable;
const assert = require('assert');

describe('serializable', ()=>{
  it('should not be undefined', ()=>{
    assert.notStrictEqual(typeof serializable, 'undefined');
  });
  it('should be a function', ()=>{
    assert.strictEqual(typeof serializable, 'function');
  });
  it('should accept 3 arguments', ()=>{
    assert.strictEqual(serializable.length, 3);
  });
  describe('Config', ()=>{
    let predicateCalled = false;
    let predicateCalledWith;
    let methodCalled = false;
    let methodCalledWith;
    const config = [{
      predicate: (target)=>{
        predicateCalled = true;
        predicateCalledWith = target;
        return true;
      },
      method: (...args)=>{
        methodCalled = true;
        methodCalledWith = args;
        args[3](args[0], args[1], args[2]);
      }
    }];
    const primativeTarget = 'string';
    const objectTarget = {
      key: 'value'
    };
    let resetTest = ()=>{
      predicateCalled = false;
      predicateCalledWith = undefined;
      methodCalled = false;
      methodCalledWith = undefined;
    };
    it('should accept an array for the config', ()=>{
      serializable(primativeTarget, resetTest, config);
    });
    it('should call the predicate function', ()=>{
      assert.strictEqual(predicateCalled, false);
      serializable(primativeTarget, ()=>{
        assert.strictEqual(predicateCalled, true);
        resetTest();
      }, config);
    });
    it('should call the method function if the predicate returns true', ()=>{
      assert.strictEqual(methodCalled, false);
      serializable(primativeTarget, ()=>{
        assert.strictEqual(methodCalled, true);
        resetTest();
      }, [{
        predicate: () => true,
        method: config[0].method
      }]);
    });
    it('should not call the method function if the predicate returns false', ()=>{
      assert.strictEqual(methodCalled, false);
      serializable(primativeTarget, ()=>{
        assert.strictEqual(methodCalled, false);
        resetTest();
      }, [{
        predicate: () => false,
        method: config[0].method
      }]);
    });
  });
  describe('Object', ()=>{
    const target = {
      key: 'value'
    };
    it('should call callback with null for error', ()=>{
      serializable(target, (e)=>{
        assert.strictEqual(e, null);  
      });
    });
    it('should call callback with an object', ()=>{
      serializable(target, (e, object)=>{
        assert.strictEqual(Object.getPrototypeOf(object), Object.prototype);  
      });
    });
    it('should not be the same object', ()=>{
      serializable(target, (e, object)=>{
        assert.notStrictEqual(object, target);  
      });
    });
    it('should have the same enumerable properties', ()=>{
      serializable(target, (e, object)=>{
        for(let key in target){
          assert.strictEqual(object.hasOwnProperty(key), true);
          assert.strictEqual(object[key], target[key]);    
        }
      });
    });
    it('should serialize to the same value', ()=>{
      serializable(target, (e, object)=>{
        assert.strictEqual(JSON.stringify(object), JSON.stringify(target));
      });
    });
    describe('Nested Objects', ()=>{
      const nested = {
        hostKey: 'hostValue',
        target,
        target2: target
      };
      it('should call the callback with the nested object', ()=>{
        serializable(nested, (e, object)=>{
          assert.notStrictEqual(object, nested);
        });
      });
      it('should not be the same nested object', ()=>{
        serializable(nested, (e, object)=>{
          assert.notStrictEqual(object.target, nested.target);
        });
      });
      it('should serialize to the nested value', ()=>{
        serializable(nested, (e, object)=>{
          assert.strictEqual(JSON.stringify(object), JSON.stringify(nested));
        });
      });
      it('should clone object references', ()=>{
        serializable(nested, (e, object)=>{
          assert.strictEqual(object.target, object.target2);
        });
      });
    });
  });
  describe('Array', ()=>{
    const target = ['index 0'];
    it('should call callback with null for error', ()=>{
      serializable(target, (e)=>{
        assert.strictEqual(e, null);  
      });
    });
    it('should call callback with an array', ()=>{
      serializable(target, (e, array)=>{
        assert.strictEqual(Array.isArray(array), true);  
      });
    });
    it('should not be the same array', ()=>{
      serializable(target, (e, array)=>{
        assert.notStrictEqual(array, target);  
      });
    });
    it('should serialize to the same value', ()=>{
      serializable(target, (e, object)=>{
        assert.strictEqual(JSON.stringify(object), JSON.stringify(target));
      });
    });
  });
  describe('Map', ()=>{
    const mapValue = [
      ['key', 'value'],
    ];
    const target = new Map(mapValue);
    it('should call callback with null for error', ()=>{
      serializable(target, (e)=>{
        assert.strictEqual(e, null);  
      });
    });
    it('should call callback with an array', ()=>{
      serializable(target, (e, array)=>{
        assert.strictEqual(Array.isArray(array), true);  
      });
    });
    it('should not be the same map', ()=>{
      serializable(target, (e, array)=>{
        assert.notStrictEqual(array, target);  
      });
    });
    it('should serialize to the same value', ()=>{
      serializable(target, (e, object)=>{
        assert.strictEqual(JSON.stringify(object), JSON.stringify(mapValue));
      });
    });
    describe('Nested Map', ()=>{
      const nestedMapValue = [
        [mapValue, {key: 'value'}],
        ['key', mapValue]
      ];
      const nested = new Map(nestedMapValue);
      it('should call the callback with the nested object', ()=>{
        serializable(nested, (e, object)=>{
          assert.notStrictEqual(object, nested);
        });
      });
      it('should not be the same nested map', ()=>{
        serializable(nested, (e, object)=>{
          assert.notStrictEqual(object[0], nestedMapValue);
          assert.notStrictEqual(object[0], nestedMapValue[0]);
          assert.notStrictEqual(object[0][1], nestedMapValue[0][1]);
          assert.notStrictEqual(object[1], nestedMapValue[1]);
          assert.notStrictEqual(object[1][1], nestedMapValue[1][1]);
        });
      });
      it('should serialize to the nested value', ()=>{
        serializable(nested, (e, object)=>{
          assert.strictEqual(JSON.stringify(object), JSON.stringify(nestedMapValue));
        });
      });
      it('should clone object references', ()=>{
        serializable(nested, (e, object)=>{
          assert.strictEqual(object[0][0], object[1][1]);
        });
      });
    });
  });
  describe('Set', ()=>{
    const setValue = ['key', 'value'];
    const target = new Set(setValue);
    it('should call callback with null for error', ()=>{
      serializable(target, (e)=>{
        assert.strictEqual(e, null);  
      });
    });
    it('should call callback with an array', ()=>{
      serializable(target, (e, array)=>{
        assert.strictEqual(Array.isArray(array), true);  
      });
    });
    it('should not be the same set', ()=>{
      serializable(target, (e, array)=>{
        assert.notStrictEqual(array, target);  
      });
    });
    it('should serialize to the same value', ()=>{
      serializable(target, (e, object)=>{
        assert.strictEqual(JSON.stringify(object), JSON.stringify(setValue));
      });
    });
  });
});