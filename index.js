const default_config = [
    {
      predicate: isArray,
      method: cloneArray
    },{
      predicate: isObject,
      method: cloneObject
    },{
      predicate: isMap,
      method: cloneTransformMap
    },{
      predicate: isSet,
      method: cloneTransformSet
    }
];

//http://www.ecma-international.org/ecma-262/6.0/index.html#sec-object.prototype.tostring
function isObject(target, type){
  type = type || 'Object';
  return Object.prototype.toString.call(target) === `[object ${type}]`;
}
 
function isArray(target){
  return Array.isArray(target);
}

function isMap(target){
  return target instanceof Map;
}

function isSet(target){
  return target instanceof Set;
}


function cloneArray(e, target, state, next){
  let l = target.length, 
    i = 0, 
    value = [],
    _next;
  _next = (e)=>{
    if(i < l){
      clone(e, target[i], state, (e, v)=>{
        value.push(v);
        i++;
        _next(e);
      });
    }else{
      next(e, value, state);
    }
  };
  _next(e);
}

function cloneObject(e, target, state, next){
  let keys = Object.keys(target),
    l = keys.length, 
    i = 0, 
    value = {},
    _next;
  _next = (e)=>{
    if(i < l){
      clone(e, target[keys[i]], state, (e, v)=>{
        value[keys[i]] = v;
        i++;
        _next(e);
      });
    }else{
      next(e, value, state);
    }
  };
  _next(e);
}

function cloneTransformMap(e, target, state, next){
  let flatten = [...target].reduce((acc, cur) => acc.concat(cur), []),
    l = flatten.length,
    i = 0,
    value = [],
    _next;
  _next = (e)=>{
    if(i < l){
      clone(e, flatten[i], state, (e, v)=>{
        value.push(v);
        i++;
        _next(e);
      });
    }else{
      next(e, value.reduce((acc, cur, i)=>{
        if(i%2 === 0){
          acc.push([cur]);
        }else{
          acc[acc.length - 1].push(cur);
        }
        return acc;
      }, []), state);
    }
  };
  _next(e);
}

function cloneTransformSet(e, target, state, next){
  cloneArray(e, [...target], state, next);
}

function mapSortConfig(config){
  return config.map(mapConfig).sort(compareConfig);
}

function mapConfig(configObject){
  return Object.assign(
    {}, 
    configObject, 
    {
      keys: configObject.keypath.split('.')
    }
  );
}

function compareConfig(a, b){
  if(a.keypath.length > b.keypath.length){
    return -1;
  }
  if(a.keypath.length < b.keypath.length){
    return 1;
  }
  return 0;
}

function keypathTraversal(target, keys){
  keys = keys.slice();
  let key = keys.slice(-1)[0],
      parent;
  if(keys.length){
    while(keys.length){
      parent = target;
      target = resolveKeypath(parent, keys);
    }
  }
  return {
    key,
    target,
    parent
  };
}

function resolveKeypath(target, keypath){
  let key = keypath.shift();
  if(typeof target[key] === 'undefined'){
    throw new Error(`Failed to resolve '${key}' on target`);
  }
  return target[key];
}

function serializable(target, cb, config){
  config = config ? config.concat(default_config) : default_config.slice();
  clone(null, target, {cache: new Map(), config}, cb);
}

function construct(target, config, cb){
  config = mapSortConfig(config);
  let cloneConfig = default_config.slice(0, 2);
  clone(null, target, {cache: new Map(), config: cloneConfig}, (e, value, state)=>{
    // make this async
    config.forEach((configObject)=>{
      let targetMap = new Map();
      const resolved = keypathTraversal(value, configObject.keys);
      let constructed = targetMap.get(resolved.target);
      if(!constructed){
        constructed = new configObject.constructor(resolved.target);
        targetMap.set(resolved.target, constructed);
      }
      resolved.parent[resolved.key] = constructed;
    });
    cb(e, value, state);
  });
}

function clone(e, target, state, next){
  let method, cached;
  if (e !== null) {
    return next(e, target, state);
  }
  for(i = 0; i < state.config.length; i++){
    if(state.config[i].predicate(target)){
      method = state.config[i].method;
      break;
    }
  }
  if(method){
    cached = state.cache.get(target);
    if(cached){
      return next(null, cached, state);
    }
    return method(null, target, state, (e, c)=>{
      state.cache.set(target, c);
      next(e, c, state);
    });
  }
  return next(null, target, state);
}

module.exports = {
  serializable,
  construct,
  clone
};