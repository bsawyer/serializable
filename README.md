[![Build Status](https://img.shields.io/travis/bsawyer/serializable.svg?style=flat-square)](https://travis-ci.org/bsawyer/serializable)
## serializable()

````js
const mapValue = [[1,2],[3,4]];

const myMap = new Map(mapValue);

serializable(myMap, (err, value)=>{
  console.log(JSON.stringify(value) === JSON.stringify(mapValue));
  // true
});
````

Supports multiple nested data types

````js
const mapValue = [
  [1,2],
  [3,['my', 'set']]
];

const myMap = new Map([
  [1,2],
  [3,new Set(['my', 'set'])]
]);

serializable(myMap, (err, value)=>{
  console.log(JSON.stringify(value) === JSON.stringify(mapValue));
  // true
});
````

Supports custom nested data types

````js
class MyClass{
  constructor(){
    this.override = false;
  }
  toJSON(){
    return {
      override: this.override
    };
  }
}

const myClass = new MyClass();

serializable(myClass, (err, value)=>{
  console.log(JSON.stringify(value))
  // {override:true}
}, [
  {
    predicate: (t) => t instanceof MyClass,
    method: (error, target, state, next) => {
      target.override = true;
      next(error, target);
    }
  }
]);
````

## construct()
````js
const target = { 
  myMap:[
    [
      'key', 
      ["set", "as", "value"]
    ]
  ]
};

construct(target, [{
    keypath: 'myMap',
    constructor: Map
  },{
    keypath: 'myMap.0.0.1',
    constructor: Set
  }
], (err, value)=>{
  console.log(value.myMap instanceof Map);
  // true
  console.log(value.myMap.get('key') instanceof Set);
  // true
});
````


## clone()

````js
const nested = [{
  object: 'value'
}];

const target = {
  nested,
  object: {
    nested
  }
};

clone(null, target, { cache: new Map(), config }, (err, value)=>{
  console.log(value);
});
````